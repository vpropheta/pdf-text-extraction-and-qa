import re
import faiss
import gradio as gr
import tempfile
from langchain.chat_models import ChatOpenAI
from langchain.chains import RetrievalQA
from langchain.embeddings import OpenAIEmbeddings
from langchain.vectorstores import FAISS
from langchain.prompts import PromptTemplate
from langchain.text_splitter import CharacterTextSplitter

class PDFProcessor:
    def __init__(self, chunk_size=1000, chunk_overlap=0):
        self.chunk_size = chunk_size
        self.chunk_overlap = chunk_overlap

    def process_pdf_content(self, pdf_content):
        if pdf_content is not None:
            # Create a list of documents using CharacterTextSplitter
            text_splitter = CharacterTextSplitter(chunk_size=self.chunk_size, chunk_overlap=self.chunk_overlap)
            documents = text_splitter.create_documents([pdf_content])
            return documents
        return []
    
class ResponseGenerator:
    def __init__(self, openai_api_key, docs):
        self.openai_api_key = openai_api_key
        self.docs = docs

    def generate_response(self, pdf_content, query_text):
        texts = PDFProcessor().process_pdf_content(pdf_content)

        if not texts:
            return "No PDF content to process."

        embeddings = OpenAIEmbeddings(openai_api_key=self.openai_api_key)

        # Create a vectorstore from documents
        db = FAISS.from_documents(texts, embeddings)
        
        # Create a retriever interface
        retriever = db.as_retriever()

        template = """You're a Brazilian tax rules generation engine

        {context}

        Question: {question}
        Answer:"""
        prompt = PromptTemplate(template=template, input_variables=["context", "question"])
        chain_type_kwargs = {"prompt": prompt}

        qa = RetrievalQA.from_chain_type(llm=ChatOpenAI(openai_api_key=self.openai_api_key, temperature=0, model="gpt-3.5-turbo-16k"), chain_type='stuff',
                                          retriever=retriever, chain_type_kwargs=chain_type_kwargs)

        response = re.sub(r"\s+", " ", qa.run(query_text))

        return response

# Define a Gradio interface for testing
def response_generator_interface(pdf_file, query_text):
    # Save the uploaded PDF file to a temporary file
    with tempfile.NamedTemporaryFile(delete=False) as temp_pdf:
        temp_pdf.write(pdf_file.read())
        temp_pdf_path = temp_pdf.name

    openai_api_key = '.....'
    generator = ResponseGenerator(openai_api_key, docs=[])

    response = generator.generate_response(temp_pdf_path, query_text)
    
    return response

# Create a Gradio interface
gr.Interface(
    fn=response_generator_interface,
    inputs=["file", "text"],
    outputs="text",
    title="PDF Text Extraction and QA",
    description="Upload a PDF file and ask a question about its content.",
).launch()